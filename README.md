# Robust Vision based Autonomous Take off and landing

*Contributors:-*
* Alessio Zanchettin
* Raghav Khanna

## Overview
Autonomous Landing is a framework that implements robust and smooth takeoff and landing for a Unmanned Aerial Vehicles (UAV) using only on-board sensors and computation and test it in a real word system. With this framework, UAVs are able to takeoff, complete the actions planned by the running application and finally landing again on the platform. Than the UAV can directly repeat these maneuvers, right after the landing, without any reset of the system. Furthermore the system is robust to light condition and tag loss, so can be used also in the outside environment without any problem.

## Branches
There are three main branches:

1. master: where there are the papers, analysis, and other info
2. for_uav: where the freamework is set to run directly on the UAVs
3. simulation: where a simulation is run and the freamework is tested on it


## Contents
The repository contains two main folders:

autonomous\_fly: ROS node, main root with

* the **source** of the code
* the **launch** files necessary to the start-up of the freamework
* the **resources** files with all the configurations

matlab: scripts for

* study of the system
* plot final results of the experiments

to_copy: files that must be copied in other directories


## UAV necessary packages
* FCU: [asctec_mav_freamework/camera_trigger](https://github.com/ethz-asl/asctec_mav_framework/tree/camera_trigger) commit [d07b4ec](https://github.com/ethz-asl/asctec_mav_framework/commit/d07b4ece70a768c9e789d42eb4240f565388211f)
* Camera: [pointgrey_camera_driver](http://wiki.ros.org/pointgrey_camera_driver)

# SIMULATION necessary packages
* Simulator: [RotorS](https://github.com/ethz-asl/rotors_simulator) commit [4f939f2](https://github.com/ethz-asl/rotors_simulator/commit/4f939f2d86cf20354144e55d83770e8815378b15)

# COMMON necessay packages
* Tracker: [ar_track_alvar](https://github.com/zalessio/ar_track_alvar) commit [6f041eb](https://github.com/zalessio/ar_track_alvar/commit/6f041ebd9003580264b8a53fe1386f41512b24a7)
* Vision Inertial Odometry [rovio/feature/improveAlignmentEquations](https://github.com/ethz-asl/rovio/tree/feature/improveAlignmentEquations) commit [e65a8e9](https://github.com/ethz-asl/rovio/commit/e65a8e9d87809e7d421e5378510b76b69d5dcb8b)
* Extended Kalman Filter: [ethzasl_msf](https://github.com/ethz-asl/ethzasl_msf) commit [d02784b](https://github.com/ethz-asl/ethzasl_msf/commit/d02784b4d7148e2adfa2777a666a57c48585f5ca)
* Controller: [mav_control/start_stop_contorller](https://github.com/ethz-asl/mav_control/tree/start_stop_controller) commit [6b3ba08](https://github.com/ethz-asl/mav_control/commit/6b3ba08aacf0f1520bd7c8cdf62faf0ebc650de4)


## Install Instructions
1. download all the necessary nodes from the list above
2. inside to\_copy take files and copy them in the respective directories
3. simulation: copy to\_copy/my_tag inside the folder /.gazebo/models
4. for_uav: copy 80-pointgraycamera.rules inside /etc/udev/rules.d

## Launch Instructions
1. ONLY MSF: roslaunch autonomous_fly uav_autonomous_fly_msf.launch
2. MSF ROVIO: roslaunch autonomous_fly uav_autonomous_fly_rovio.launch

## Test
TAKEOFF:

* rosservice call /mav_name/maneuver "maneuver: 'takeoff'"
* rostopic pub /mav_name/command/pose geometry\_msgs/PoseStamped '{header: {stamp: now, frame_id: "world"}, pose: {position: {x: 0.0, y: 0.0, z: 0.6}, orientation: {w: 1.0}}}'

LANDING:

* rosservice call /mav_name/maneuver "maneuver: 'landing'"
* rostopic pub /name_uav/command/pose geometry\_msgs/PoseStamped '{header: {stamp: now, frame_id: "world"}, pose: {position: {x: 0.0, y: 0.0, z: 0.0}, orientation: {w: 1.0}}}'

## REPORT
for more detail on the project look at robust-vision-based.pdf
