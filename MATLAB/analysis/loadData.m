% fly 1 2016-01-29-18-05-00.bag
% multiple fly 2016-01-29-18-08-24.bag
%rovio ok stateestimationerrorworldfixed_2016-02-26-11-09-35.bag
% stateestimationerrorworldfixedlossbase_2016-02-26-12-06-16.bag
clear all;
fileFolder = '';
fileName = 'stateestimationerrorworldfixedlossbase_2016-02-26-12-38-30.bag';
filePath = strcat(fileFolder,fileName);
bagselect = rosbag(filePath);

%%
initalTime = bagselect.StartTime;
finalTime = bagselect.EndTime;
bagMsgs = select(bagselect, 'Time', [initalTime,finalTime]);
MSFOdometryBag = readMessages(select(bagMsgs, 'Topic', '/nibbio/msf_core/odometry'));
VICONOdometryBag = readMessages(select(bagMsgs, 'Topic', '/nibbio/vrpn_client/estimated_odometry'));
ROVIOPoseBag = readMessages(select(bagMsgs, 'Topic', '/nibbio/body_map/pose'));
MSFPose = zeros(size(MSFOdometryBag,1),7);
VICONPose = zeros(size(VICONOdometryBag,1),7);
ROVIOPose = zeros(size(ROVIOPoseBag,1),7);

%%
for i=1:size(MSFOdometryBag)
    MSFPose(i,:) = [MSFOdometryBag{i,1}.Pose.Pose.Position.X, ... 
                    MSFOdometryBag{i,1}.Pose.Pose.Position.Y, ...
                    MSFOdometryBag{i,1}.Pose.Pose.Position.Z, ...
                    MSFOdometryBag{i,1}.Pose.Pose.Orientation.W, ... 
                    MSFOdometryBag{i,1}.Pose.Pose.Orientation.X, ... 
                    MSFOdometryBag{i,1}.Pose.Pose.Orientation.Y, ...
                    MSFOdometryBag{i,1}.Pose.Pose.Orientation.Z];
end

for i=1:size(VICONOdometryBag)
    VICONPose(i,:) = [VICONOdometryBag{i,1}.Pose.Pose.Position.X, ... 
                      VICONOdometryBag{i,1}.Pose.Pose.Position.Y, ...
                      VICONOdometryBag{i,1}.Pose.Pose.Position.Z, ...
                      VICONOdometryBag{i,1}.Pose.Pose.Orientation.W, ... 
                      VICONOdometryBag{i,1}.Pose.Pose.Orientation.X, ... 
                      VICONOdometryBag{i,1}.Pose.Pose.Orientation.Y, ...
                      VICONOdometryBag{i,1}.Pose.Pose.Orientation.Z];
end

for i=1:size(ROVIOPoseBag)
    ROVIOPose(i,:) = [ROVIOPoseBag{i,1}.Pose.Position.X, ... 
                      ROVIOPoseBag{i,1}.Pose.Position.Y, ...
                      ROVIOPoseBag{i,1}.Pose.Position.Z, ...
                      ROVIOPoseBag{i,1}.Pose.Orientation.W, ... 
                      ROVIOPoseBag{i,1}.Pose.Orientation.X, ... 
                      ROVIOPoseBag{i,1}.Pose.Orientation.Y, ...
                      ROVIOPoseBag{i,1}.Pose.Orientation.Z];
end

ROVIOVICONSwiched = false;
if(size(ROVIOPose,1) > size(VICONPose,1))
    tmpPose = ROVIOPose;
    ROVIOPose = VICONPose;
    VICONPose = tmpPose;
    tmpBag = ROVIOPoseBag;
    ROVIOPoseBag = VICONOdometryBag;
    VICONOdometryBag = tmpBag;
    ROVIOVICONSwiched = true;
end