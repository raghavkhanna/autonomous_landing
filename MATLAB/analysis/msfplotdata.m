% indexmatchMSF = zeros(size(MSFPose,1)+1,1);
% indexmatchMSF(1) = 3;
% 
% for i=1:size(MSFPose,1)
%     delay = 10e20;
%     for j=max(1,(indexmatchMSF(i)-2)):size(VICONOdometryBag,1)
%         diff = abs((MSFOdometryBag{i,1}.Header.Stamp.Sec - VICONOdometryBag{j,1}.Header.Stamp.Sec)*1e9 + MSFOdometryBag{i,1}.Header.Stamp.Nsec - VICONOdometryBag{j,1}.Header.Stamp.Nsec);
%         if(diff < delay)
%             delay = diff;
%             indexmatchMSF(i+1) = j;
%         else
%             break;
%         end
%     end
% end

%%
for i=200:400
    translationMSF(i,1:3) = VICONPose(indexmatchMSF(i+1),1:3) - MSFPose(i,1:3);
end
translationMSF = mean(translationMSF);
vicontranslateMSF = VICONPose(indexmatchMSF(2:end),1:3) - repmat(translationMSF,size(MSFPose,1),1);

%%
errorMSF = (MSFPose(:,1:3) - vicontranslateMSF).^2;
sqrt(mean(errorMSF))

%%
delta = (finalTime - initalTime)/(size(MSFPose,1)-1);
timeMSF = 0:delta:(finalTime - initalTime);

%%
figure
subplot(3,1,1)
plot(timeMSF,vicontranslateMSF(:,1),'Color',[0    0.4470    0.7410]);
hold on
plot(timeMSF,MSFPose(:,1),'Color',[0.8500    0.3250    0.0980]);
title('x')
xlabel('time')
subplot(3,1,2)
plot(timeMSF,vicontranslateMSF(:,2),'Color',[0    0.4470    0.7410]);
hold on
plot(timeMSF,MSFPose(:,2),'Color',[0.8500    0.3250    0.0980]);
title('y')
xlabel('time')
subplot(3,1,3)
plot(timeMSF,vicontranslateMSF(:,3),'Color',[0    0.4470    0.7410]);
hold on
plot(timeMSF,MSFPose(:,3),'Color',[0.8500    0.3250    0.0980]);
title('z')
xlabel('time')