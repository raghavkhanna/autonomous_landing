% indexMatchROVIO = zeros(size(ROVIOPose,1)+1,1);
% indexMatchROVIO(1) = 3;
% for i=1:size(ROVIOPose,1)
%     delay = 10e20;
%     for j=max(1,(indexMatchROVIO(i)-2)):size(VICONOdometryBag,1)
%         diff = abs((ROVIOPoseBag{i,1}.Header.Stamp.Sec - VICONOdometryBag{j,1}.Header.Stamp.Sec)*1e9 + ROVIOPoseBag{i,1}.Header.Stamp.Nsec - VICONOdometryBag{j,1}.Header.Stamp.Nsec);
%         if(diff < delay)
%             delay = diff;
%             indexMatchROVIO(i+1) = j;
%         else
%             break;
%         end
%     end
% end

%%
for i=1:100
    translationROVIO(i,1:3) = VICONPose(indexMatchROVIO(i+1),1:3) - ROVIOPose(i,1:3);
end
translationROVIO = mean(translationROVIO);
vicontranslateROVIO = VICONPose(indexMatchROVIO(2:end),1:3) - repmat(translationROVIO,size(ROVIOPose,1),1);

%%
errorROVIO = ROVIOPose(:,1:3) - vicontranslateROVIO(:,1:3);
RMSERovio = sqrt(mean(errorROVIO.^2))
MaxERovio = max(errorROVIO)

%%
delta = (finalTime - initalTime)/(size(ROVIOPose,1)-1);
timeROVIO = 0:delta:(finalTime - initalTime);

%%
figure
subplot(3,1,1)
plot(timeROVIO,vicontranslateROVIO(:,1),'Color',[0    0.4470    0.7410]);
hold on
plot(timeROVIO,ROVIOPose(:,1),'Color',[0.4660    0.6740    0.1880]);
title('x')
xlabel('time')
ylabel('space')
if(ROVIOVICONSwiched)
    legend('ROVIO','Mo-cap')
else
    legend('Mo-cap','ROVIO')
end
subplot(3,1,2)
plot(timeROVIO,vicontranslateROVIO(:,2),'Color',[0    0.4470    0.7410]);
hold on
plot(timeROVIO,ROVIOPose(:,2),'Color',[0.4660    0.6740    0.1880]);
title('y')
xlabel('time')
ylabel('space')
subplot(3,1,3)
plot(timeROVIO,vicontranslateROVIO(:,3),'Color',[0    0.4470    0.7410]);
hold on
plot(timeROVIO,ROVIOPose(:,3),'Color',[0.4660    0.6740    0.1880]);
title('z')
xlabel('time')
ylabel('space')