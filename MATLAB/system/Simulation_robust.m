global x0;
global xf;
global tc;
global Tf;
global noise;

noisy = true ;

tc = 0.1;
Tf = 15;
x0 = [0,0,0.085,0,0,0,0,0,0,0,0,0];
xf = [0,0,0.1,0,0,0,0,0,0,0,0,0];
if(noisy)
    noise = [(rand(3,Tf*5+1)-0.5)*0.01;
             (rand(3,Tf*5+1)-0.5)*0.01;
             (rand(1,Tf*5+1)-0.5)*0.01;
             (rand(1,Tf*5+1)-0.5)*0.01;
             (rand(1,Tf*5+1)-0.5)*0.01;
             (rand(3,Tf*5+1)-0.5)*0.01];
else
    noise = (rand(12,Tf*5+1)-0.5)*0.001;
end
     
global tprev;
global dintegx;
global dintegy;
global dintegz;
tprev = 0;
dintegx = 0;
dintegy = 0;
dintegz = 0;

[T,Y] = ode45(@SP_system_robust,[0,Tf],x0);

figure
plot(T,x0(1:3)'*(T < tc)' + xf(1:3)'*(T > tc)',T,Y(:,1:3));
legend('ref_x','ref_y','ref_z','x','y','z');
figure
plot(T,Y(:,4:6));
legend('v_x','v_y','v_z');
%figure
%plot(T,Y(:,7:9));
%legend('phi','theta','psi');
%figure
%plot(T,Y(:,10:12));
%legend('v_{phi}','v_{theta}','v_{psi}');

