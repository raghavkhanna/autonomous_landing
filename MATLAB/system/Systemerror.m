function e = Systemerror(x)

global Kpx; global Kdx; global Kix;
global Kpy; global Kdy; global Kiy;
global Kphip; global Kphid;
global Kthetap; global Kthetad;
Kpx = 0.3; Kdx = 1.5; Kix = 0.01; 
Kpy = 3.05; Kdy = 15.05; Kiy = 0.1;
Kphip = x(1); Kphid = x(2);
Kthetap = x(3); Kthetad = x(4);
global x0;
global xf;
global tc;
global Tf;
global noise;
global tprev;
global dintegx;
global dintegy;
global dintegz;

tc = 0.01;
Tf = 10;
x0 = [0,0,0.085,0,0,0,0,0,0,0,0,0];
xf = [0,0,0.5,0,0,0,0,0,0,0,0,0];
e = 0;
for i =1:30
    noise = (rand(12,Tf*5+1)-0.5)*0.01;
    tprev = 0;
    dintegx = 0;
    dintegy = 0;
    dintegz = 0;

    [~,Y] = ode45(@SP_system_optimize,[0,Tf],x0);

    e = e + Y(:,1)'*Y(:,1) + Y(:,2)'*Y(:,2);
end
e = e/30;

% figure
% plot(T,x0(1:3)'*(T < tc)' + xf(1:3)'*(T > tc)',T,Y(:,1:3));
% legend('ref_x','ref_y','ref_z','x','y','z');
end