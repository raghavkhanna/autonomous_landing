function f = SP_system_robust(t,x)

global xf;
global x0;
global tc;
global Tf;
global noise;

if (t < tc)
    xfinal = x0;
else
    xfinal = xf;
end

noise_t = noise(:,floor((size(noise,2)-1)*t/Tf)+1);

global tprev;
global dintegx;
global dintegy;
global dintegz;

g = 9.81;
m = 1.56779;
Ixx = 3.47563e-2;
Iyy = 3.47563e-2;
Izz = 9.77e-2;

Kxp = 1;
Kxd = 1.8;
Kxi = 0.1;

Kyp = 9;
Kyd = 18;
Kyi = 0.1;

Kzp = 4;
Kzd = 5;
Kzi = 0;

Kphip = 16;
Kphid = 8;

Kthetap = 16;
Kthetad = 8;

Kpsip = 6;
Kpsid = 3.5;

dposx = xfinal(1)-x(1) + noise_t(1);
dposy = xfinal(2)-x(2) + noise_t(2);
dposz = xfinal(3)-x(3) + noise_t(3);
dvelx = xfinal(4)-x(4) + noise_t(4);
dvely = xfinal(5)-x(5) + noise_t(5);
dvelz = xfinal(6)-x(6) + noise_t(6);

if(t >= tprev)
    dintegx = dintegx + dposx*(t-tprev);
    dintegy = dintegy + dposy*(t-tprev);
    dintegz = dintegz + dposz*(t-tprev);
end

dx = Kxp*dposx + Kxd*dvelx + Kxi*dintegx;
dy = Kyp*dposy + Kyd*dvely + Kyi*dintegy;
dz = Kzp*dposz + Kzd*dvelz + Kzi*dintegz;

aaa = (dx*sin(xfinal(9))-dy*cos(xfinal(9)))/(dx^2+dy^2+(dz+g)^2);
if aaa  > 1
    aaa = 1;
end
if aaa < -1
    aaa = -1;
end
xfinal(7) = asin(aaa);
xfinal(8) = atan2((dx*cos(xfinal(9))+dy*sin(xfinal(9))),dz+g);

% my system is ok, but my controller uses noisy information
dphi = xfinal(7)-x(7) + noise_t(7);
dtheta = xfinal(8)-x(8) + noise_t(8);
dpsi = xfinal(9)-x(9) + noise_t(9);
dvelphi = xfinal(10)-x(10) + noise_t(10);
dveltheta = xfinal(11)-x(11) + noise_t(11);
dvelpsi = xfinal(12)-x(12) + noise_t(12);

%Thrust UP sum wi
T = (dz + g)*m/(cos(xfinal(7))*cos(xfinal(8))) ;
%Rotation Xaxis w1 + 2w2 + w3 - w4 - 2w5 - w6
Mphi = (Kphip*dphi + Kphid*dvelphi)*Iyy;
%Rotation Yaxis w1 - w3 - w4 + w6
Mtheta = (Kthetap*dtheta + Kthetad*dveltheta)*Ixx;
%Rotation Zaxis w1 - w2 + w3 - w4 + w5 - w6
Mpsi = (Kpsip*dpsi + Kpsid*dvelpsi)*Izz; 
 
f = zeros(12,1);
%      1 2 3  4  5  6    7    8    9  10 11 12
% x = [x,y,z, x',y',z', phi,theta,psi, p, q, r] 
%     |     world     |     world    |   body
f(1) = x(4); 
f(2) = x(5);
f(3) = x(6); 
f(4) = (cos(x(7))*sin(x(8))*cos(x(9)) + sin(x(7))*sin(x(9)))*T/m ; 
f(5) = (cos(x(7))*sin(x(8))*sin(x(9)) - sin(x(7))*cos(x(9)))*T/m ; 
f(6) = -g + cos(x(7))*cos(x(8))*T/m ;
f(7) = x(10) + sin(x(7))*tan(x(8))*x(11) + cos(x(7))*tan(x(8))*x(12);
f(8) = cos(x(7))*x(11) - sin(x(7))*x(12);
f(9) = (sin(x(7))/cos(x(8)))*x(11) + (cos(x(7))/cos(x(8)))*x(12);
f(10)= ((Iyy-Izz)/Ixx)*x(11)*x(12) + Mphi/Ixx   ;
f(11)= ((Izz-Ixx)/Iyy)*x(10)*x(12) + Mtheta/Iyy ;
f(12)= ((Ixx-Iyy)/Izz)*x(10)*x(11) + Mpsi/Izz;


tprev = t;
end