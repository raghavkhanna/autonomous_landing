

q0 = a(:,5);
q1 = a(:,6);
q2 = a(:,7);
q3 = a(:,8);

k = 1;
j = 1;
for i =1:size(a,1)
    if(a(i,1) == 0)
        x(k) = a(i,2);
        y(k) = a(i,3);
        z(k) = a(i,4);
        phi(k) = atan2(2*(q0(i)*q1(i)+q2(i)*q3(i)),1-2*(q1(i)*q1(i)+q2(i)*q2(i)));
        theta(k) = asin(2*(q0(i)*q2(i)-q3(i)*q1(i)));
        psi(k) = atan2(2*(q0(i)*q3(i)+q1(i)*q3(i)),1-2*(q2(i)*q2(i)+q3(i)*q3(i)));
        time(k) = a(i,9);
        k = k+1;
    else
        real_x(j) = a(i,2);
        real_y(j) = a(i,3);
        real_z(j) = a(i,4);
        real_phi(j) = atan2(2*(q0(i)*q1(i)+q2(i)*q3(i)),1-2*(q1(i)*q1(i)+q2(i)*q2(i)));
        real_theta(j) = asin(2*(q0(i)*q2(i)-q3(i)*q1(i)));
        real_psi(j) = atan2(2*(q0(i)*q3(i)+q1(i)*q3(i)),1-2*(q2(i)*q2(i)+q3(i)*q3(i)));
        real_time(j) = a(i,9);
        j = j +1;
    end
end

i = 1;
for k =1:size(time,2)
    for j=1:size(real_time,2)
        if(real_time(j) == time(k))
            err_x(i) = (x(k) - real_x(j)); 
            err_y(i) = (y(k) - real_y(j)); 
            err_z(i) = z(k) - real_z(j); 
            err_phi(i) = (phi(k) - real_phi(j)); 
            err_theta(i) = (theta(k) - real_theta(j)); 
            err_psi(i) = (psi(k) - real_psi(j)); 
            i = i +1;
        end
    end
end

figure
plot(time(1:107),err_x(1:107),time(1:107),err_y(1:107))
legend('err_x','err_y')
figure
plot(time(1:107),err_phi(1:107),time(1:107),err_theta(1:107),time(1:107),err_psi(1:107))
legend('err_{phi}','err_{theta}','err_{psi}')

        