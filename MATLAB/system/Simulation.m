global x0;
global xf;
global tc;

time_bounds = [0,15];
x0 = [0,0,0.085,0,0,0,0,0,0,0,0,0];
tc = 5;
xf = [0.5,1,2,0,0,0,0,0,0,0,0,0];

[T,Y] = ode45(@SP_system,time_bounds,x0);

close all
figure
plot(T,x0(1:3)'*(T < 5)' + xf(1:3)'*(T > 5)',T,Y(:,1:3));
legend('ref_x','ref_y','ref_z','x','y','z');
figure
plot(T,Y(:,4:6));
legend('v_x','v_y','v_z');
figure
plot(T,Y(:,7:9));
legend('phi','theta','psi');
figure
plot(T,Y(:,10:12));
legend('v_{phi}','v_{theta}','v_{psi}');

