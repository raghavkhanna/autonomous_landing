
function f = SP_system(t,x)

global xf;
global x0;
global tc;

if (t < tc)
    xfinal = x0;
else
    xfinal = xf;
end

g = 9.81;
m = 1.56779;
Ixx = 3.47563e-2;
Iyy = 3.47563e-2;
Izz = 9.77e-2;

Kxp = 0.30;
Kxd = 0.95;
Kyp = 3.0;
Kyd = 9.5;
Kzp = 4;
Kzd = 5;
Kphip = 8;
Kphid = 4;
Kthetap = 8;
Kthetad = 4;
Kpsip = 3;
Kpsid = 1.75;

dposx = xfinal(1)-x(1);
dposy = xfinal(2)-x(2);
dposz = xfinal(3)-x(3);
dvelx = xfinal(4)-x(4);
dvely = xfinal(5)-x(5);

dvelz = xfinal(6)-x(6);
dx = Kxp*dposx + Kxd*dvelx;
dy = Kyp*dposy + Kyd*dvely;
dz = Kzp*dposz + Kzd*dvelz;

xfinal(7) = asin((dx*sin(xfinal(9))-dy*cos(xfinal(9)))/(dx^2+dy^2+(dz+g)^2));
xfinal(8) = atan2((dx*cos(xfinal(9))+dy*sin(xfinal(9))),dz+g);

dphi = xfinal(7)-x(7);
dtheta = xfinal(8)-x(8);
dpsi = xfinal(9)-x(9);
dvelphi = xfinal(10)-x(10);
dveltheta = xfinal(11)-x(11);
dvelpsi = xfinal(12)-x(12);

%Thrust UP sum wi
T = (dz + g)*m/(cos(xfinal(7))*cos(xfinal(8))) ;
%Rotation Xaxis w1 + 2w2 + w3 - w4 - 2w5 - w6
Mphi = (Kphip*dphi + Kphid*dvelphi)*Iyy;
%Rotation Yaxis w1 - w3 - w4 + w6
Mtheta = (Kthetap*dtheta + Kthetad*dveltheta)*Ixx;
%Rotation Zaxis w1 - w2 + w3 - w4 + w5 - w6
Mpsi = (Kpsip*dpsi + Kpsid*dvelpsi)*Izz; 
 
f = zeros(12,1);
%      1 2 3  4  5  6    7    8    9  10 11 12
% x = [x,y,z, x',y',z', phi,theta,psi, p, q, r] 
%     |     world     |     world    |   body
f(1) = x(4); 
f(2) = x(5);
f(3) = x(6); 
f(4) = (cos(x(7))*sin(x(8))*cos(x(9)) + sin(x(7))*sin(x(9)))*T/m ; 
f(5) = (cos(x(7))*sin(x(8))*sin(x(9)) - sin(x(7))*cos(x(9)))*T/m ; 
f(6) = -g + cos(x(7))*cos(x(8))*T/m ;
f(7) = x(10) + sin(x(7))*tan(x(8))*x(11) + cos(x(7))*tan(x(8))*x(12);
f(8) = cos(x(7))*x(11) - sin(x(7))*x(12);
f(9) = (sin(x(7))/cos(x(8)))*x(11) + (cos(x(7))/cos(x(8)))*x(12);
f(10)= ((Iyy-Izz)/Ixx)*x(11)*x(12) + Mphi/Ixx   ;
f(11)= ((Izz-Ixx)/Iyy)*x(10)*x(12) + Mtheta/Iyy ;
f(12)= ((Ixx-Iyy)/Izz)*x(10)*x(11) + Mpsi/Izz;

end